import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService, authServiceInitProvider } from './services';
import { authInterceptorProviders } from './interceptors';

import { FakeApiService } from './fake-api';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,

    // Fake Auth API: Remove this in real apps
    HttpClientInMemoryWebApiModule.forRoot(FakeApiService),
  ],
  providers: [
    AuthService,
    authServiceInitProvider,
    ...authInterceptorProviders,
  ],
})
export class CoreModule {}
