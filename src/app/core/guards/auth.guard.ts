import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  Router,
} from '@angular/router';

import { environment } from '../../../environments/environment';

import { AuthService, FakeAuthService } from '../services/';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private fakeAuthService: FakeAuthService,
    private router: Router
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (environment.isBackendActive) {
      if (this.authService.authState$.getValue().isLoggedIn) {
        // logged in so return true
        return true;
      }
    } else {
      const currentUser = this.fakeAuthService.currentUserValue;
      if (currentUser) {
        // logged in so return true
        return true;
      }
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], {
      queryParams: { returnUrl: state.url },
    });
    return false;
  }
}
