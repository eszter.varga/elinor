export interface User {
  id: number;
  name: string;
  userName: string;
  customer: string;
  role: string;
  email: string;
  phone: number;
  maxFailedRetryNumber: number;
  failedRetryNumber?: number;
  dashboardGroup: string;
  userLocked?: boolean;
  allDataFilled: boolean;
}

export const UserDefaults: User = {
  id: 0,
  name: '',
  userName: '',
  customer: '',
  role: '',
  email: '',
  phone: 0,
  maxFailedRetryNumber: 0,
  failedRetryNumber: 0,
  dashboardGroup: '',
  userLocked: false,
  allDataFilled: false,
};
