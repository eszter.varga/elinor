export interface AppUser {
  name: string;
  username: string;
  customername: string;
  role: string;
  email: string;
}
