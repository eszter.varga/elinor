export interface Token {
  exp: number;
  iat: number;
  sub: string;
}
