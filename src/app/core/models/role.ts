export interface Role {
  id: number;
  name: string;
  status: string;
  options: Array<string>;
  creationDate: number | Date;
  createdBy: string;
  allDataFilled: boolean;
}
