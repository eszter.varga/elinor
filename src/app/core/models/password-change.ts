export interface PasswordChange {
  password: string;
  passwordAgain: string;
  passwordOld: string;
  username: string;
}

export const PasswordChangeDefaults: PasswordChange = {
  password: '',
  passwordAgain: '',
  passwordOld: '',
  username: '',
};
