import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { User, PasswordChange } from '../../core';
import { ToastService } from './toast.service';

@Injectable({ providedIn: 'root' })
export class UserService {
  // private url = environment.apiUrl + `/api/user`;

  private url = '/assets/mocks';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + sessionStorage.getItem('token'),
    }),
  };

  constructor(private http: HttpClient, private toastService: ToastService) {}

  getUsers(): Observable<User[]> {
    return this.http
      .get<User[]>(`${this.url}/user.json`, this.httpOptions)
      .pipe(
        map((users) => {
          users.forEach((user) => this.backendDataToFrontendData(user));
          return users;
        }),
        catchError((e) => this.toastService.handleError(e))
      );
  }

  getUserGroups() {
    return this.http
      .get<object>(`${this.url}/user-groups.json`, this.httpOptions)
      .pipe(catchError((e) => this.toastService.handleError(e)));
  }

  addUser(user: User) {
    user = this.frontendDataToBackendData(user);
    return this.http.post(`${this.url}` + '/save', user, this.httpOptions).pipe(
      map(() => this.toastService.showSuccess(`User is added`)),
      catchError((e) => this.toastService.handleError(e))
    );
  }

  updateUser(user: User): Observable<any> {
    user = this.frontendDataToBackendData(user);
    return this.http.post(`${this.url}/update`, user, this.httpOptions).pipe(
      map(() => this.toastService.showSuccess(`User is updated`)),
      catchError((e) => this.toastService.handleError(e))
    );
  }

  updateUserGroups(userGroups: any): Observable<any> {
    return this.http
      .post(`${this.url}/update`, userGroups, this.httpOptions)
      .pipe(
        map(() => this.toastService.showSuccess(`User groups are updated`)),
        catchError((e) => this.toastService.handleError(e))
      );
  }

  resetPassword(passwordChangeData: PasswordChange) {
    return this.http
      .post(`${this.url}/reset-password`, passwordChangeData, this.httpOptions)
      .pipe(
        map(() => {
          this.toastService.showSuccess('Password is changed');
        }),
        catchError((e) => this.toastService.handleError(e))
      );
  }

  deleteUser(id: number): Observable<any> {
    return this.http.post(`${this.url}/delete?id=${id}`, this.httpOptions).pipe(
      map(() => this.toastService.showSuccess(`User is deleted`)),
      catchError((e) => this.toastService.handleError(e))
    );
  }

  backendDataToFrontendData(user: User): User {
    return user;
  }

  frontendDataToBackendData(user: User): User {
    return user;
  }
}
