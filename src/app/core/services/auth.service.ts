import { HttpClient, HttpParams } from '@angular/common/http';
import { APP_INITIALIZER, Injectable, Provider } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom, Observable, BehaviorSubject, of } from 'rxjs';
import { filter, take, map, finalize, catchError } from 'rxjs/operators';

import { ConfigService, TokenStorageService } from './';
import {
  AuthState,
  AuthUser,
  AccessData,
  TokenStatus,
  initialState,
} from '../models';

@Injectable()
export class AuthService {
  private hostUrl: string;
  private clientId: string;
  private clientSecret: string;
  public state = initialState;
  public authState$ = new BehaviorSubject<AuthState>(this.state);
  public isLoadingLogin$ = new BehaviorSubject<boolean>(false);
  public hasLoginError$ = new BehaviorSubject<boolean>(false);

  constructor(
    private router: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private configService: ConfigService,
    private tokenStorageService: TokenStorageService
  ) {
    this.hostUrl = this.configService.getAPIUrl();
    const authConfig = this.configService.getAuthSettings();
    this.clientId = authConfig.clientId;
    this.clientSecret = authConfig.secretId;
  }

  /**
   * Returns a promise that waits until
   * refresh token and get auth user
   *
   * @returns {Promise<AuthState>}
   */
  init(): Promise<AuthState> {
    this.refreshToken().subscribe();

    const state$ = of(this.state).pipe(
      filter(
        (auth) =>
          auth.refreshTokenStatus === TokenStatus.INVALID ||
          (auth.refreshTokenStatus === TokenStatus.VALID && !!auth.user)
      ),
      take(1)
    );

    return lastValueFrom(state$);
  }

  /**
   * Performs a request with user credentials
   * in order to get auth tokens
   *
   * @param {string} username
   * @param {string} password
   * @returns Observable<AccessData>
   */
  login(username: string, password: string): Observable<AccessData> {
    return this.http
      .post<AccessData>(`${this.hostUrl}/api/auth/login`, {
        client_id: this.clientId,
        client_secret: this.clientSecret,
        grant_type: 'password',
        username,
        password,
      })
      .pipe(
        map((accessData) => {
          // save tokens
          this.tokenStorageService.saveTokens(
            accessData.access_token,
            accessData.refresh_token
          );
          // trigger login success action
          return this.loginSuccess(accessData);
        }),
        catchError((error) => of(this.loginFailure(error)))
      );
  }

  /**
   * Set login state
   *
   * @returns void
   */
  loginState(): void {
    this.state = {
      ...this.state,
      accessTokenStatus: TokenStatus.VALIDATING,
      isLoading: true,
      hasError: false,
    };
    this.authState$.next(this.state);
  }

  /**
   * Handle login success
   *
   * @param {AccessData} accessData
   * @returns AccessData
   */
  loginSuccess(accessData: AccessData): AccessData {
    this.router.navigateByUrl(
      this.activatedRoute.snapshot.queryParams['returnUrl'] || '/'
    );
    this.loginSuccessState();
    this.getAuthUser().subscribe();
    return accessData;
  }

  /**
   * Set login success state
   *
   * @returns void
   */
  loginSuccessState(): void {
    this.state = {
      ...this.state,
      isLoggedIn: true,
      isLoading: false,
      accessTokenStatus: TokenStatus.VALID,
      refreshTokenStatus: TokenStatus.VALID,
    };
    this.authState$.next(this.state);
  }

  /**
   * Handle login failure
   *
   * @param {_error}
   * @returns AccessData
   */
  loginFailure(_error: any): AccessData {
    this.tokenStorageService.removeTokens();
    this.loginFailureState();
    return {} as AccessData;
  }

  /**
   * Set login failure state
   *
   * @returns void
   */
  loginFailureState(): void {
    this.state = {
      ...this.state,
      isLoading: false,
      accessTokenStatus: TokenStatus.INVALID,
      refreshTokenStatus: TokenStatus.INVALID,
      hasError: true,
    };
    this.authState$.next(this.state);
  }

  /**
   * Performs a request for logout authenticated user
   *
   * @param {('all' | 'allButCurrent' | 'current')} [clients='current']
   * @returns Observable<void>
   */
  logout(
    clients: 'all' | 'allButCurrent' | 'current' = 'current'
  ): Observable<void> {
    this.router.navigateByUrl('/');
    const params = new HttpParams().append('clients', clients);
    return this.http
      .get<void>(`${this.hostUrl}/api/auth/logout`, { params })
      .pipe(
        finalize(() => {
          this.logoutState();
          this.tokenStorageService.removeTokens();
        })
      );
  }

  /**
   * Set logout state
   *
   * @returns void
   */
  logoutState(): void {
    this.state = initialState;
    this.authState$.next(this.state);
  }

  /**
   * Asks for a new access token given
   * the stored refresh token
   *
   * @returns {Observable<AccessData>}
   */
  refreshToken(): Observable<AccessData> {
    const refreshToken = this.tokenStorageService.getRefreshToken();
    if (!refreshToken) {
      return of(this.refreshTokenFailure());
    }

    return this.http
      .post<AccessData>(`${this.hostUrl}/api/auth/login`, {
        client_id: this.clientId,
        client_secret: this.clientSecret,
        grant_type: 'refresh_token',
        refresh_token: refreshToken,
      })
      .pipe(
        map((accessData) => {
          // save tokens
          this.tokenStorageService.saveTokens(
            accessData.access_token,
            accessData.refresh_token
          );
          // trigger refresh token success action
          return this.refreshTokenSuccess(accessData);
        }),
        catchError(() => of(this.refreshTokenFailure()))
      );
  }

  /**
   * Set refesh token request state
   *
   * @returns void
   */
  refreshTokenRequestState(): void {
    this.state = {
      ...this.state,
      refreshTokenStatus: TokenStatus.VALIDATING,
    };
    this.authState$.next(this.state);
  }

  /**
   * Handle refresh token success
   *
   * @param {AccessData} accessData
   * @returns AccessData
   */
  refreshTokenSuccess(accessData: AccessData): AccessData {
    this.refreshTokenSuccessState();
    this.getAuthUser().subscribe();
    return accessData;
  }

  /**
   * Set refesh token success state
   *
   * @returns void
   */
  refreshTokenSuccessState(): void {
    this.state = {
      ...this.state,
      isLoggedIn: true,
      isLoading: false,
      accessTokenStatus: TokenStatus.VALID,
      refreshTokenStatus: TokenStatus.VALID,
    };
    this.authState$.next(this.state);
  }

  /**
   * Handle refresh token failure
   *
   * @returns AccessData
   */
  refreshTokenFailure(): AccessData {
    this.refreshTokenFailureState();
    this.tokenStorageService.removeTokens();
    return {} as AccessData;
  }

  /**
   * Set refesh token failure state
   *
   * @returns void
   */
  refreshTokenFailureState(): void {
    this.state = {
      ...this.state,
      isLoading: false,
      accessTokenStatus: TokenStatus.INVALID,
      refreshTokenStatus: TokenStatus.INVALID,
      hasError: true,
    };
    this.authState$.next(this.state);
  }

  /**
   * Returns authenticated user
   * based on saved access token
   *
   * @returns {Observable<AuthUser>}
   */
  getAuthUser(): Observable<AuthUser> {
    return this.http.get<AuthUser>(`${this.hostUrl}/api/users/me`).pipe(
      map((user) => this.getAuthUserSuccess(user)),
      catchError(() => of(this.getAuthUserFailure()))
    );
  }

  /**
   * Handle auth user success
   *
   * @param {AuthUser} user
   * @returns AuthUser
   */
  getAuthUserSuccess(user: AuthUser): AuthUser {
    this.getAuthUserSuccessState(user);
    return user;
  }

  /**
   * Set auth user success state
   *
   * @param {AuthUser} user
   * @returns void
   */
  getAuthUserSuccessState(user: AuthUser): void {
    this.state = { ...this.state, user: user };
    this.authState$.next(this.state);
  }

  /**
   * Handle auth user failure
   *
   * @returns AuthUser
   */
  getAuthUserFailure(): AuthUser {
    this.getAuthUserFailureState();
    return {} as AuthUser;
  }

  /**
   * Set auth user failure state
   *
   * @returns void
   */
  getAuthUserFailureState(): void {
    this.state = initialState;
    this.authState$.next(this.state);
  }

  /**
   * Performs a request with user credentials
   * in order to get auth tokens
   *
   * @param {string} username
   * @param {string} password
   * @returns Observable<AccessData>
   */
  register(username: string, password: string): Observable<AccessData> {
    return this.http
      .post<AccessData>(`${this.hostUrl}/api/auth/register`, {
        client_id: this.clientId,
        client_secret: this.clientSecret,
        grant_type: 'password',
        username,
        password,
      })
      .pipe(
        map((accessData) => {
          // save tokens
          this.tokenStorageService.saveTokens(
            accessData.access_token,
            accessData.refresh_token
          );
          // trigger register success action
          return this.registerSuccess(accessData);
        }),
        catchError((error) => of(this.registerFailure(error)))
      );
  }

  /**
   * Set register state
   *
   * @returns void
   */
  registerState(): void {
    this.state = {
      ...this.state,
      accessTokenStatus: TokenStatus.VALIDATING,
      isLoading: true,
      hasError: false,
    };
    this.authState$.next(this.state);
  }

  /**
   * Handle register success
   *
   * @param {AccessData} accessData
   * @returns AccessData
   */
  registerSuccess(accessData: AccessData): AccessData {
    this.router.navigateByUrl(
      this.activatedRoute.snapshot.queryParams['returnUrl'] || '/'
    );
    this.registerSuccessState();
    this.getAuthUser().subscribe();
    return accessData;
  }

  /**
   * Set register success state
   *
   * @returns void
   */
  registerSuccessState(): void {
    this.state = {
      ...this.state,
      isLoggedIn: true,
      isLoading: false,
      accessTokenStatus: TokenStatus.VALID,
      refreshTokenStatus: TokenStatus.VALID,
    };
    this.authState$.next(this.state);
  }

  /**
   * Handle register failure
   *
   * @param {_error}
   * @returns AccessData
   */
  registerFailure(_error: any): AccessData {
    this.tokenStorageService.removeTokens();
    this.registerFailureState();
    return {} as AccessData;
  }

  /**
   * Set register failure state
   *
   * @returns void
   */
  registerFailureState(): void {
    this.state = {
      ...this.state,
      isLoading: false,
      accessTokenStatus: TokenStatus.INVALID,
      refreshTokenStatus: TokenStatus.INVALID,
      hasError: true,
    };
    this.authState$.next(this.state);
  }
}

export const authServiceInitProvider: Provider = {
  provide: APP_INITIALIZER,
  useFactory: (authService: AuthService) => () => authService.init(),
  deps: [AuthService],
  multi: true,
};
