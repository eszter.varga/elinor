import { Injectable, TemplateRef } from '@angular/core';

import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toasts: any[] = [];

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast: any) {
    this.toasts = this.toasts.filter((t) => t !== toast);
  }

  showSuccess(template: string) {
    this.show(template, {
      classname: 'bg-success text-light',
      delay: 5000,
    });
  }

  showError(template: string) {
    this.show(template, {
      classname: 'bg-danger text-light',
      delay: 5000,
    });
  }

  public handleError(error: HttpErrorResponse) {
    let message = 'Error occured!';
    if (error && error.error && error.error.error) {
      message = error.error.error;
    } else if (error && error.status) {
      message = 'Error happened, status code: ' + error.status;
    }
    this.showError(message);
    return throwError('');
  }
}
