import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
  {
    id: 1,
    label: 'MENUITEMS.MENU.TEXT',
    isTitle: true,
  },
  {
    id: 2,
    label: 'MENUITEMS.PAGES.TEXT',
    icon: 'ri-pages-line',
    collapseid: 'sidebarPages',
    subItems: [
      {
        id: 3,
        label: 'MENUITEMS.PAGES.LIST.STARTER',
        link: '/pages/starter',
        parentId: 2,
      },
    ],
  },
];
