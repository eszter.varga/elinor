import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-language-menu',
  templateUrl: './language-menu.component.html',
  styleUrls: ['./language-menu.component.scss'],
})
export class LanguageMenuComponent implements OnInit {
  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }

  ngOnInit(): void {
    const userLang = navigator.language;
    if (userLang == 'hu-HU') {
      this.translate.setDefaultLang('hu');
    }
  }

  useLanguage(language: string) {
    this.translate.use(language);
  }
}
