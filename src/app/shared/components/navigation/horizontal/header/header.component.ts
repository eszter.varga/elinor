import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }

  ngOnInit(): void {
    let userLang = navigator.language;
    if (userLang == 'hu-HU') {
      this.translate.setDefaultLang('hu');
    }
  }

  useLanguage(language: string) {
    this.translate.use(language);
  }
}
