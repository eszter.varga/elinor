import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PasswordChangeModalComponent } from '../../password-change-modal/password-change-modal.component';
import { AuthService } from '../../../../core/';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
})
export class UserMenuComponent {
  constructor(public dialog: MatDialog, private authService: AuthService) {}

  openDialog() {
    const dialogRef = this.dialog.open(PasswordChangeModalComponent);
  }

  logout() {
    this.authService.logout();
  }
}
