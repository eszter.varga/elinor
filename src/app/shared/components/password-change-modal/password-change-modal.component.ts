import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {
  PasswordChange,
  PasswordChangeDefaults,
  UserService,
  AuthService,
} from '../../../core';

@Component({
  selector: 'app-password-change-modal',
  templateUrl: './password-change-modal.component.html',
  styleUrls: ['./password-change-modal.component.scss'],
})
export class PasswordChangeModalComponent {
  public passwords: PasswordChange = PasswordChangeDefaults;
  constructor(
    private userService: UserService,
    private authService: AuthService,
    public dialogRef: MatDialogRef<PasswordChangeModalComponent>
  ) {}

  resetPassword() {
    this.passwords.username = this.authService.state.user!.userName;
    this.userService.resetPassword(this.passwords).subscribe(() => {
      this.passwords = PasswordChangeDefaults;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
