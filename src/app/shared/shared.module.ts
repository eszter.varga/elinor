import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './design/material/material.module';
import { IconModule } from './design/icon/icon.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { BasicToolbarComponent } from './components/navigation/horizontal/basic-toolbar/basic-toolbar.component';
import { LanguageMenuComponent } from './components/navigation/language-menu/language-menu.component';
import { UserMenuComponent } from './components/navigation/user-menu/user-menu.component';
import { HeaderComponent } from './components/navigation/horizontal/header/header.component';
import { ToastComponent } from './components/toast/toast.component';
import { RoleDirective } from './directives/role.directive';
import { PasswordChangeModalComponent } from './components/password-change-modal/password-change-modal.component';

@NgModule({
  declarations: [
    HeaderComponent,
    ToastComponent,
    RoleDirective,
    UserMenuComponent,
    PasswordChangeModalComponent,
    LanguageMenuComponent,
    BasicToolbarComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    NgbModule,
    IconModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
  ],
  exports: [
    MaterialModule,
    NgbModule,
    IconModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    TranslateModule,
    HeaderComponent,
    ToastComponent,
    LanguageMenuComponent,
    BasicToolbarComponent,
  ],
})
export class SharedModule {}
