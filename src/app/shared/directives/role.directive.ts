import {
  Input,
  OnInit,
  Directive,
  ViewContainerRef,
  TemplateRef,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../../core';

@Directive({
  selector: '[userRole]',
})
export class RoleDirective implements OnInit {
  // the role the user must have
  @Input()
  role: Array<string> = [];
  private subscription: Subscription[] = [];

  /**
   * @param {ViewContainerRef} viewContainerRef
   * 	-- the location where we need to render the templateRef
   * @param {TemplateRef<any>} templateRef
   *   -- the templateRef to be potentially rendered
   * @param {AuthService} authService
   *   -- will give us access to the roles a user has
   */
  constructor(
    private viewContainerRef: ViewContainerRef,
    private templateRef: TemplateRef<any>,
    private authService: AuthService
  ) {}

  public ngOnInit(): void {
    this.subscription.push(
      this.authService.authState$.subscribe((state) => {
        if (!state || !state.user || !state.user.roles) {
          // Remove element from DOM
          this.viewContainerRef.clear();
        }
        // user Role are checked by a Roles mention in DOM
        const idx = state.user!.roles.findIndex(
          (element) => this.role.indexOf(element) !== -1
        );
        if (idx < 0) {
          this.viewContainerRef.clear();
        } else {
          // appends the ref element to DOM
          this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
      })
    );
  }

  /**
   * on destroy cancels the API if its fetching.
   */
  public ngOnDestroy(): void {
    this.subscription.forEach((subscription: Subscription) =>
      subscription.unsubscribe()
    );
  }
}
