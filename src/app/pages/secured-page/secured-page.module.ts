import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecuredPageRoutingModule } from './secured-page-routing.module';
import { SecuredPageComponent } from './secured-page.component';

@NgModule({
  declarations: [SecuredPageComponent],
  imports: [CommonModule, SecuredPageRoutingModule],
})
export class SecuredPageModule {}
